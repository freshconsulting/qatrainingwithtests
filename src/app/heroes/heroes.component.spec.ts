import { Component, ElementRef, Input, NO_ERRORS_SCHEMA } from "@angular/core";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { of } from "rxjs";
import { Hero } from "../hero";
import { HeroService } from "../hero.service";
import { HeroesComponent } from "./heroes.component"

describe('HeroesComponent', () => {
    let component: HeroesComponent;
    let HEROES;
    let spyHeroService;

    beforeAll(() => {
        spyHeroService = jasmine.createSpyObj(['getHeroes', 'addHero', 'deleteHero'])

        component = new HeroesComponent(spyHeroService);
    });

    beforeEach(() => {
        HEROES = [
            {id:1, name: 'SpiderDude', strength: 8},
            {id:2, name: 'Wonderful Woman', strength: 24},
            {id:3, name: 'SuperDude', strength: 55}
        ];

        //mockHeroService = jasmine.createSpyObj(['getHeroes', 'addHero', 'deleteHero'])

        //component = new HeroesComponent(mockHeroService);
    })

    describe('add', () => {

        it('should add a new hero to the list', () => {
            // Arrange
            component.heroes = HEROES;
            spyHeroService.addHero.and.returnValue(of({id:4, name: 'AquaDude', strength: 11}));

            // Act
            component.add('AquaDude');

            // Assert
            expect(component.heroes.length).toBe(4);
        })

        it('should add the indicated hero to the list', () => {
            // Arrange
            component.heroes = HEROES;
            spyHeroService.addHero.and.returnValue(of({id:4, name: 'AquaDude', strength: 11}));

            // Act
            component.add('AquaDude');

            // Assert
            let hero = component.heroes.find(hero => hero.name === 'AquaDude');
            expect(hero).toBeDefined();
        })
    })

    describe('delete', () => {

        // This one only tests the line `this.heroes = this.heroes.filter(h => h !== hero);`
        it('should remove the indicated hero from the heroes list', () => {
            component.heroes = HEROES;
            spyHeroService.deleteHero.and.returnValue(of(true)); //We want to return any observable, an easy one is of(true)

            component.delete(HEROES[2]);

            expect(component.heroes.length).toBe(2);
        })

        // This other one tests the other line `this.heroService.deleteHero(hero).subscribe();`
        it('should call deleteHero', () => {
            spyHeroService.deleteHero.and.returnValue(of(true)); //We want to return any observable, an easy one is of(true)
            component.heroes = HEROES;

            component.delete(HEROES[2]);

            //expect(mockHeroService.deleteHero).toHaveBeenCalled();
            expect(spyHeroService.deleteHero).toHaveBeenCalledWith(HEROES[2]);
        })
    })

    describe('Shallow Integration Tests', () => {
        let fixture: ComponentFixture<HeroesComponent>;
        let element: HTMLElement;
        
        beforeEach(() => {
            TestBed.configureTestingModule({
                declarations: [HeroesComponent],
                // We need to register the service that is being injected, but the mocked one.
                providers: [
                    { provide: HeroService, useValue: spyHeroService }
                ],
                schemas: [NO_ERRORS_SCHEMA] // We ignore the child component
            })
            fixture = TestBed.createComponent(HeroesComponent);
            element = fixture.nativeElement;
        })

        it('should set heroes correctly from the service', () => {
            // Arrange
            spyHeroService.getHeroes.and.returnValue(of(HEROES)) // Since this is a jasmine.createSpyObj we can tell what to return, in this case an observable
            fixture.detectChanges(); // This triggers ngOnInit() and, since the template uses the heroes, we need to run this to bind changes.

            // Act

            // Assert
            expect(fixture.componentInstance.heroes.length).toBe(3);
        })

        it('should call add when clicking on the button', () => {
            // Arrange
            const hero = { name: 'AquaDude', strength: 11 }
            spyHeroService.getHeroes.and.returnValue(of(HEROES))
            // The next line was required only during the workshop
            //spyHeroService.addHero.and.returnValue(of(hero));
            fixture.detectChanges();
            //const input: HTMLInputElement = fixture.componentInstance.heroName.nativeElement;
            const element: HTMLElement = fixture.nativeElement
            const input = element.querySelector('input');
            input.value = 'AquaDude';

            // Act
            const button: HTMLButtonElement = element.querySelector('button');
            button.click();

            // Assert
            expect(spyHeroService.addHero).toHaveBeenCalledWith(hero);
        })

        it('should add the indicated hero when clicking on the button', () => {
            // Arrange
            spyHeroService.getHeroes.and.returnValue(of(HEROES))
            fixture.detectChanges();
            const input: HTMLInputElement = fixture.componentInstance.heroName.nativeElement;
            input.value = 'AquaDude';
            spyHeroService.addHero.and.returnValue(of({id:4, name: 'AquaDude', strength: 11}));

            // Act
            const button: HTMLButtonElement = element.querySelector('button');
            button.click();

            // Assert
            expect(fixture.componentInstance.heroes.length).toBe(4);
            
            console.log(`fixture.nativeElement.querySelectorAll('li').length`,fixture.nativeElement.querySelectorAll('li').length); // 3

            fixture.detectChanges(); // Another option here is to use fixture.autoDetectChanges() only once.
            console.log(`fixture.nativeElement.querySelectorAll('li').length`,fixture.nativeElement.querySelectorAll('li').length); // 4

            expect(fixture.nativeElement.querySelectorAll('li').length).toBe(4);
        })
    })

    describe('Shallow Tests without NO_ERRORS_SCHEMA', () => {
        let fixture: ComponentFixture<HeroesComponent>;
        
        @Component({
            selector: 'app-hero',
            template: '<div></div>',
        })
        class FakeHeroesComponent {
            @Input() hero: Hero;
        }

        beforeEach(() => {
            TestBed.configureTestingModule({
                declarations: [
                    HeroesComponent,
                    FakeHeroesComponent, // <-- Now without NO_ERRORS_SCHEMA we need to declare our child component
                ],
                providers: [
                    { provide: HeroService, useValue: spyHeroService }
                ],
                //schemas: [NO_ERRORS_SCHEMA] // If we use this we ignore the child component
            })
            fixture = TestBed.createComponent(HeroesComponent);
        })

        it('should set heroes correctly from the service', () => {
            spyHeroService.getHeroes.and.returnValue(of(HEROES)) // Since this is a jasmine.createSpyObj we can tell what to return, in this case an observable
            fixture.detectChanges(); // Since the template uses the heroes, we need to run this to bind changes.

            expect(fixture.componentInstance.heroes.length).toBe(3);
        })

        it('should create one li for each hero', () => {
            spyHeroService.getHeroes.and.returnValue(of(HEROES))
            fixture.detectChanges();

            expect(fixture.nativeElement.querySelectorAll('li').length).toBe(3);
        })
    })
})
