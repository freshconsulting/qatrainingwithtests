import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Team } from './team';

@Injectable()
export class TeamService {

	constructor(private http: HttpClient) {
	}

	getTeams(): Observable<Team[]> {
		return of([
            {name: 'Fresh Consulting'},
            {name: 'Harbor Foods'}
        ]);
	}
}
