import { of } from "rxjs";
import { TeamListComponent } from "./team-list.component";
import { TeamService } from "./team.service";

describe('Team List Component', () => {

    it('should get the teams (stub)', () => {
        // Arrange
        const service = new TeamService(undefined);
        const component = new TeamListComponent(service);

        const teamsData = [{ name: 'Fresh Consulting' }];
        service.getTeams = () => of(teamsData); // Stub

        // Act
        component.ngOnInit(); // Since this is a unit test, we need to trigger ngOnInit() manually. This is not required when running fixture.detectChanges() which will initialize the component.

        // Assert
        component.teams$.subscribe(teams => {
            expect(teams).toEqual(teamsData);
        });
    })
    
    it('should get the teams (spy)', () => {

        // Arrange
        const service = new TeamService(undefined);
        const component = new TeamListComponent(service);

        const spy = spyOn(service, 'getTeams'); // Spy

        // Act
        component.ngOnInit();

        // Assert
        expect(spy).toHaveBeenCalled();
    })
});
