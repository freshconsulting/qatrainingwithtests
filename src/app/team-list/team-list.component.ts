import { Component, OnInit } from '@angular/core';
import { TeamService } from './team.service';
import { Team } from './team';
import { Observable } from 'rxjs';

@Component({
	selector: 'app-team-list',
	template: `
		<ul>
			<li *ngFor="let team of teams$ | async">
				{{team.name}}
			</li>
		</ul>`,
})
export class TeamListComponent implements OnInit {
	teams$: Observable<Team[]>;

	constructor(private teamService: TeamService) {
    }

    ngOnInit() {
        this.teams$ = this.teamService.getTeams();
    }
}
