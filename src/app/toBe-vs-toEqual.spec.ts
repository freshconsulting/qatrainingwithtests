describe('Testing toBe vs toEqual', () => {
    let obj1 = { a: 1 };
    let obj2 = { a: 1 };
    let obj3 = { a: 2 };

    xit('obj1 to be obj2', () => {
        expect(obj1).toBe(obj2); // Fails
    })

    it('obj1 to equal obj2', () => {
        expect(obj1).toEqual(obj2); // Pass
    })

    xit('obj1 to equal obj3', () => {
        expect(obj1).toEqual(obj3); // Fails
    })
})
