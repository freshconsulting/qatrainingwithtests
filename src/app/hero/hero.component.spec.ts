import { NO_ERRORS_SCHEMA } from "@angular/core";
import { ComponentFixture, TestBed } from "@angular/core/testing"
import { HeroComponent } from "./hero.component"

describe('Hero Component (shallow tests)', () => {
    let fixture: ComponentFixture<HeroComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [HeroComponent],
            schemas: [NO_ERRORS_SCHEMA] // If we remove this we'll have a console error about the routerLink in the template, to solve it add `imports:[RouterModule.forRoot([])]` here
        });
        fixture = TestBed.createComponent(HeroComponent);
    })

    describe('Isolated Unit Tests', () => {

        it('should have the correct hero', () => {
            fixture.componentInstance.hero = { id: 1, name: 'SuperDude', strength: 3 };
            // fixture.detectChanges(); // Not needed here

            expect(fixture.componentInstance.hero.name).toEqual('SuperDude');
        })

        it('should render the hero name in an anchor tag', () => {
            // Arrange
            fixture.componentInstance.hero = { id: 1, name: 'SuperDude', strength: 3 };
            fixture.detectChanges(); //bind this ^ change to the template

            // Act

            // Assert
            let heroName = fixture.nativeElement.querySelector('a').textContent;
            expect(heroName).toContain('SuperDude');
        })
    })

    describe('Shallow Integration Tests', () => {

        it('should trigger delete event on button click', () => {
            // Arrange
            fixture.componentInstance.hero = { id: 1, name: 'SuperDude', strength: 3 };
            fixture.detectChanges(); //bind this ^ change to the template
            const spy = spyOn(fixture.componentInstance.delete, 'next');
            //spyOn(fixture.componentInstance.delete, 'next');

            // Act
            fixture.nativeElement.querySelector('button.delete').click();

            // Assert 
            expect(spy).toHaveBeenCalled();
            //expect(fixture.componentInstance.delete.next).toHaveBeenCalled();
        })
    })
})
