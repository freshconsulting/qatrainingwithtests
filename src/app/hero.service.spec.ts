import { inject, TestBed } from "@angular/core/testing"
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing"
import { HeroService } from "./hero.service"
import { MessageService } from "./message.service";
import { Hero } from "./hero";

describe('Hero Service', () => {
    let spyMessageService;
    let heroService: HeroService;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        spyMessageService = jasmine.createSpyObj(['add']);

        /**
         * Looking at the constructor of hero.service.ts we know that we have to provide
         * an HttpClient and a MessageService, we do this by importing HttpClientTestingModule
         * and providing a mock of the MessageService.
         */
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                HeroService,
                { provide: MessageService, useValue: spyMessageService }
            ]
        })

        heroService = TestBed.inject(HeroService);
        httpTestingController = TestBed.inject(HttpTestingController);
    })

    afterEach(() => {
        // After every test, assert that there are no more pending requests.
        httpTestingController.verify();
    });

    it('should be created', inject([HeroService], (service: HeroService) => {
        expect(service).toBeTruthy();
    }))

    it('should be created too', () => {
        expect(heroService).toBeDefined();
    })

    it('should retrieve heroes from the API via GET', () => {
        const dummyHeroes: Hero[] = [
            {id:1, name: 'SpiderDude', strength: 8},
            {id:2, name: 'Wonderful Woman', strength: 24},
            {id:3, name: 'SuperDude', strength: 55}
        ];

        heroService.getHeroes().subscribe(heroes => {
            expect(heroes.length).toBe(3);
            expect(heroes).toEqual(dummyHeroes);
        });
        // Now we need to trigger this^ request and we control what to return with the HttpTestingController

        const request = httpTestingController.expectOne(heroService.heroesUrl);

        expect(request.request.method).toBe('GET');

        // Fire the request and send what we want to receive
        request.flush(dummyHeroes);
    })

    it('should retrieve the hero by id from the API via GET', () => {
        const dummyHero: Hero = {id:4, name: 'AquaDude', strength: 11};

        heroService.getHero(4).subscribe(hero => {
            expect(hero.id).toBe(4);
            expect(hero).toBe(dummyHero);
        });

        const request = httpTestingController.expectOne(`${heroService.heroesUrl}/4`);

        expect(request.request.method).toBe('GET');

        request.flush(dummyHero);
    })

    it('should call post with the correct url', () => {
        const dummyHero: Hero = {id:4, name: 'AquaDude', strength: 11};

        heroService.addHero(dummyHero).subscribe(hero => {
            expect(hero.id).toBe(4);
            expect(hero).toBe(dummyHero);
        });

        const request = httpTestingController.expectOne(heroService.heroesUrl);

        expect(request.request.method).toBe('POST');

        request.flush(dummyHero);
    })
})
